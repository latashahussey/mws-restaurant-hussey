var gulp = require('gulp'),
  minifyHTML = require('gulp-htmlmin'),
  minifyCSS = require('gulp-clean-css'),
  uglify = require('gulp-uglify-es').default,
  concat = require('gulp-concat'),
  jshint = require('gulp-jshint'),
  eslint = require('gulp-eslint'),
  babel = require('gulp-babel'),
  stylish = require('jshint-stylish'),
  imagemin = require('gulp-imagemin'),
  responsive = require('gulp-responsive-images'),
  jpegtran = require('imagemin-jpegtran'),
  optipng = require('imagemin-optipng'),
  autoprefixer = require('gulp-autoprefixer'),
  paths = {
    cssSrc: 'src/css/',
    cssDest: 'dist/css/',
    htmlSrc: 'src/',
    htmlDest: 'dist/',
    jsSrc: 'src/js/',
    jsDest: 'dist/js/',
    imgSrc: 'src/img',
    imgDest: 'dist/img'
  },
  browserSync = require('browser-sync').create(),
  watch = require('gulp-watch');

/* GULP TASKS */

// Check gulpfile for errors
gulp.task('jshint', function() {
  return gulp.src('gulpfile.js')
    .pipe(jshint())
    .pipe(jshint.reporter(stylish));
});

// Grab all css files then minify and move them to destination
gulp.task('minifyCSS', function() {
  return gulp.src(paths.cssSrc + '*.css')
    .pipe(autoprefixer())
    .pipe(minifyCSS())
    .pipe(concat('style.min.css'))
    .pipe(gulp.dest(paths.cssDest))
    .pipe(browserSync.stream());
});

// Grab all js files then minify, merge, and move them to destination
gulp.task('uglify', function() {
  return gulp.src(paths.jsSrc + '*.js')
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError())
    .pipe(babel())
    .pipe(uglify())
    .pipe(gulp.dest(paths.jsDest));
});

// Grab all html files, then move to destintation
gulp.task('copy-html', function() {
  return gulp.src(paths.htmlSrc + '*.html')
    .pipe(minifyHTML({ collapseWhitespace: true, removeComments: true }))
    .pipe(gulp.dest(paths.htmlDest));
});
// Copy manifest, then move to destintation
gulp.task('copy-manifest', function() {
  return gulp.src(paths.htmlSrc + 'manifest.json')
    .pipe(gulp.dest(paths.htmlDest));
});
// Move service worker to destintation
gulp.task('copy-sw', function() {
  return gulp.src(paths.htmlSrc + 'serviceWorker.js')
    .pipe(gulp.dest(paths.htmlDest));
});

// Grab all image files, compress, then move to destintation
gulp.task('copy-images', function() {
  return gulp.src(paths.imgSrc + '/*')
    .pipe(imagemin([
      jpegtran({ progressive: true }),
      optipng({ optimizationLevel: 5 })
    ]))
    .pipe(responsive({
      '*.jpg': [{
        width: 350,
        height: 250,
        crop: true,
        quality: 60
      }],
      '*.png': [{
        width: 300,
        height: 200,
        crop: true,
        quality: 60
      }]
    }))
    .pipe(gulp.dest(paths.imgDest));
});

// Setup browser-sync server
/*gulp.task('browser-sync', function() {
  browserSync.init({
    server: './dist',
    cors: true
  });
});*/

// Watch for css or changes to images, then run tasks
gulp.task('watch', function() {
  // Endless stream mode
  gulp.watch(paths.cssSrc + '*.css', ['minifyCSS']);
  gulp.watch(paths.jsSrc + '**/*.js', ['uglify']);
  gulp.watch(paths.htmlSrc + '*.html', ['copy-html']);
  gulp.watch(paths.htmlSrc + 'serviceWorker.js', ['copy-sw']);
  gulp.watch(paths.htmlSrc + 'manifest.json', ['copy-manifest']);
  gulp.watch(paths.imgSrc + '/*', ['copy-images']);
});

// Run all gulp tasks
gulp.task('default', ['jshint', 'minifyCSS', 'uglify', 'copy-html', 'copy-manifest', 'copy-sw', 'copy-images', 'watch']);