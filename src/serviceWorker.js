var restaurantsStaticCache = 'restaurants-static-v2';
var restaurantsImgsCache = 'restaurants-imgs';
var allCaches = [
  restaurantsStaticCache,
  restaurantsImgsCache
];

// Cache site assets 
self.addEventListener('install',
  function(event) {
    event.waitUntil(
      caches.open(restaurantsStaticCache).then(function(cache) {
        return cache.addAll([
          '/',
          'css/style.min.css',
          'https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.min.css',
          'fonts/didact-gothic-v11-latin-regular.woff2',
          'fonts/roboto-v18-latin-300.woff2',
          'fonts/roboto-v18-latin-700.woff2',
          'fonts/roboto-v18-latin-regular.woff2',
          'js/dbhelper.js',
          'js/idb.js',
          'js/main.js',
          'js/restaurant_info.js',
          'index.html',
          'restaurant.html',
          'manifest.json',
          'favicon.ico'
        ]);
      })

    );
  });

self.addEventListener('activate', function(event) {
  event.waitUntil(
    caches.keys().then(function(cacheNames) {
      return Promise.all(
        cacheNames.filter(function(cacheName) {
          return cacheName.startsWith('restaurants-') &&
            !allCaches.includes(cacheName)
        }).map(function(cacheName) {
          return caches.delete(cacheName);
        })
      );
    })
  );
});


// Log requests made from the parent page.
// Pull request from cache, if available
self.addEventListener('fetch', function(event) {
  var requestUrl = new URL(event.request.url);
  if (requestUrl.origin === location.origin) {
    if (requestUrl.pathname === '/') {
      event.respondWith(caches.match('/'))
      return;
    }
  }
  if (requestUrl.pathname.startsWith('/dist/img/')) {
    event.respondWith(servePhoto(event.request));
    return;
  }
  event.respondWith(
    caches.match(event.request, { ignoreSearch: true }).then(function(response) {
      return response || fetch(event.request);
    })
  );
});

// Try retrieving photos from cache, otherwise grab images from network
function servePhoto(request) {
  return caches.open(restaurantsImgsCache).then(function(cache) {
    return cache.match(request).then(function(response) {
      if (response) return response;

      return fetch(request).then(function(networkResponse) {
        cache.put(request, networkResponse.clone());
        return networkResponse;
      });
    });
  });
}



// Wait for sync event, if connection available try to process form
self.addEventListener('sync', function(event) {
  if (event.tag == 'reviewsSync') {
    event.waitUntil(DBHelper.processForm())
  }
})