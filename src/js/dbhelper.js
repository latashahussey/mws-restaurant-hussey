/**
 * Common database helper functions.
 */

var dbPromise;
let currentRestaurantReviews;
const port = 1337 // Change this to your server port

class DBHelper {

  static createRestaurantDb() {
    // Create the database 
    // Callback handler called when new version of database is created
    return idb.open('restaurant_reviews', 1, function(upgradeDb) {
      // Create the object store to store information about the restaurants
      upgradeDb.createObjectStore('restaurants', { keyPath: 'id' });
      upgradeDb.createObjectStore('reviews', { keyPath: 'id' });
    });
  }

  /**
   * Database URL to retrieve restaurants.
   * Change this to restaurants.json file location on your server.
   */
  static get DATABASE_URL() {
    return `http://localhost:${port}/restaurants`;
  }

  /**
  * Database URL to retrieve reviews.
  * Change this to restaurants.json file location on your server.
  */
  static get REVIEWS_DATABASE_URL() {
    return `http://localhost:${port}/reviews`;
  }


  /**
   * Use transaction to make sure objectStore creation is finished before
   * writing to database
  */
  static getRestaurantsFromDb() {
    dbPromise = DBHelper.createRestaurantDb();
    return dbPromise.then(function(db) {
      // Check to see if database already exists, if not skip steps that follow
      if (!db)
        return;

      // Create transaction
      var tx = db.transaction('restaurants');
      // Create objectStore
      var restaurantStore = tx.objectStore('restaurants');

      // Grab everything from indexedDb store
      return restaurantStore.getAll();
    })
  }

  static getReviewsFromDb() {
    dbPromise = DBHelper.createRestaurantDb();
    return dbPromise.then(function(db) {
      // Check to see if database already exists, if not skip steps that follow
      if (!db)
        return;

      // Create transaction
      var tx = db.transaction('reviews');
      // Create objectStore
      var reviewStore = tx.objectStore('reviews');

      // Grab everything from indexedDb store
      return reviewStore.getAll();
    })
  }



  /**
   * Fetch all restaurants.
   */
  static fetchRestaurants(callback) {

    DBHelper.getRestaurantsFromDb().then(function(restaurants) {
      // Check to see if there are any results
      if (restaurants.length > 0) {
        return callback(null, restaurants)
      } else {

        // Grab any updated information from the network
        fetch(DBHelper.DATABASE_URL,
          {
            credentials: 'same-origin'
          }).then(
            function(response) {

              return response.json();
            }
          ).then(function(restaurants) {
            dbPromise.then(function(db) {
              if (!db) return db;

              // Create transaction
              var tx = db.transaction('restaurants', 'readwrite');
              //  Add all restaurants to objectStore
              var restaurantStore = tx.objectStore('restaurants');
              restaurants.forEach(function(restaurant) {
                restaurantStore.put(restaurant);
              })
              return callback(null, restaurants);
            }).catch(function(error) {
              return callback(error, null);
            });
          });
      }
    })


  }
  /**
   * Fetch all reviews.
   */
  static fetchReviews(callback) {

    DBHelper.getReviewsFromDb().then(function(reviews) {
      // Check to see if there are any results
      if (reviews.length > 0) {
        return callback(null, reviews)
      } else {

        // Grab any updated information from the network
        fetch(DBHelper.REVIEWS_DATABASE_URL,
          {
            credentials: 'same-origin'
          }).then(
            function(response) {
              return response.json();
            }
          ).then(function(reviews) {
            dbPromise.then(function(db) {
              if (!db) return db;

              // Create transaction
              var tx = db.transaction('reviews', 'readwrite');
              //  Add all reviews to objectStore
              var reviewStore = tx.objectStore('reviews');
              reviews.forEach(function(review) {
                reviewStore.put(review);
              })
              return callback(null, reviews);
            }).catch(function(error) {
              return callback(error, null);
            });
          });
      }
    })


  }

  /**
   * Fetch a restaurant by its ID.
   */
  static fetchRestaurantById(id, callback) {
    // fetch all restaurants with proper error handling.
    DBHelper.fetchRestaurants((error, restaurants) => {
      if (error) {
        callback(error, null);
      } else {
        const restaurant = restaurants.find(r => r.id == id);
        if (restaurant) { // Got the restaurant
          callback(null, restaurant);
        } else { // Restaurant does not exist in the database
          callback('Restaurant does not exist', null);
        }
      }
    });
  }
  /**
   * Fetch a restaurant by its ID.
   */
  static fetchReviewsByRestaurantId(id, callback) {
    // fetch all reviews with proper error handling.
    DBHelper.fetchReviews((error, reviews) => {
      if (error) {
        callback(error, null);
      } else {
        currentRestaurantReviews = reviews.filter(r => r.restaurant_id == id);

        if (currentRestaurantReviews) { // Got the reviews
          callback(null, currentRestaurantReviews);
        } else { // Restaurant does not exist in the database
          callback('Restaurant does not exist.', null);
        }
      }
    });
  }

  /**
   * Fetch restaurants by a cuisine type with proper error handling.
   */
  static fetchRestaurantByCuisine(cuisine, callback) {
    // Fetch all restaurants  with proper error handling
    DBHelper.fetchRestaurants((error, restaurants) => {
      if (error) {
        callback(error, null);
      } else {
        // Filter restaurants to have only given cuisine type
        const results = restaurants.filter(r => r.cuisine_type == cuisine);
        callback(null, results);
      }
    });
  }

  /**
   * Fetch restaurants by a neighborhood with proper error handling.
   */
  static fetchRestaurantByNeighborhood(neighborhood, callback) {
    // Fetch all restaurants
    DBHelper.fetchRestaurants((error, restaurants) => {
      if (error) {
        callback(error, null);
      } else {
        // Filter restaurants to have only given neighborhood
        const results = restaurants.filter(r => r.neighborhood == neighborhood);
        callback(null, results);
      }
    });
  }

  /**
   * Fetch restaurants by a cuisine and a neighborhood with proper error handling.
   */
  static fetchRestaurantByCuisineAndNeighborhood(cuisine, neighborhood, callback) {
    // Fetch all restaurants
    DBHelper.fetchRestaurants((error, restaurants) => {
      if (error) {
        callback(error, null);
      } else {
        let results = restaurants
        if (cuisine != 'all') { // filter by cuisine
          results = results.filter(r => r.cuisine_type == cuisine);
        }
        if (neighborhood != 'all') { // filter by neighborhood
          results = results.filter(r => r.neighborhood == neighborhood);
        }
        callback(null, results);
      }
    });
  }

  /**
   * Fetch all neighborhoods with proper error handling.
   */
  static fetchNeighborhoods(callback) {
    // Fetch all restaurants
    DBHelper.fetchRestaurants((error, restaurants) => {
      if (error) {
        callback(error, null);
      } else {
        // Get all neighborhoods from all restaurants
        const neighborhoods = restaurants.map((v, i) => restaurants[i].neighborhood)
        // Remove duplicates from neighborhoods
        const uniqueNeighborhoods = neighborhoods.filter((v, i) => neighborhoods.indexOf(v) == i)
        callback(null, uniqueNeighborhoods);
      }
    });
  }

  /**
   * Fetch all cuisines with proper error handling.
   */
  static fetchCuisines(callback) {
    // Fetch all restaurants
    DBHelper.fetchRestaurants((error, restaurants) => {
      if (error) {
        callback(error, null);
      } else {
        // Get all cuisines from all restaurants
        const cuisines = restaurants.map((v, i) => restaurants[i].cuisine_type)
        // Remove duplicates from cuisines
        const uniqueCuisines = cuisines.filter((v, i) => cuisines.indexOf(v) == i)
        callback(null, uniqueCuisines);
      }
    });
  }

  /**
   * Restaurant page URL.
   */
  static urlForRestaurant(restaurant) {
    return (`./restaurant.html?id=${restaurant.id}`);
  }

  /**
   * Restaurant image URL.
   */
  static imageUrlForRestaurant(restaurant) {
    return (`./img/${restaurant.photograph}.jpg`);
  }

  /**
   * Map marker for a restaurant.
   */
  static mapMarkerForRestaurant(restaurant, map) {
    const marker = new google.maps.Marker({
      position: restaurant.latlng,
      title: restaurant.name,
      url: DBHelper.urlForRestaurant(restaurant),
      map: map,
      animation: google.maps.Animation.DROP
    });
    return marker;
  }
  /** Favorite Restaurant */
  static favoriteRestaurant(restaurant, favoriteStatus) {
    return fetch(`${DBHelper.DATABASE_URL}` + '\\' + restaurant + '\\?is_favorite=' + `${favoriteStatus}`, {
      method: 'PUT'
    }).then(
      function(response) {
        return response.json();
      }
    ).then(function(restaurantFavorite) {
      dbPromise.then(function(db) {
        if (!db) return db;

        // Create transaction
        var tx = db.transaction('restaurants', 'readwrite');
        //  Add all restaurants to objectStore
        var restaurantStore = tx.objectStore('restaurants');
        restaurantStore.put(restaurantFavorite)
      });
      return restaurantFavorite;
    })
  }

  /** Process form submission */
  static processForm(formData) {

    return fetch(`${DBHelper.REVIEWS_DATABASE_URL}`, {
      body: JSON.stringify(formData),
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'content-type': 'application/json'
      },
      method: 'POST',
      mode: 'cors'
    }).then(
      function(response) {
        return response.json();
      }
    ).then(function(userReview) {
      dbPromise.then(function(db) {
        if (!db) return db;

        // Create transaction
        var tx = db.transaction('reviews', 'readwrite');
        //  Add all reviews to objectStore
        var reviewStore = tx.objectStore('reviews');
        reviewStore.put(userReview)
      });
      return userReview;
    }).catch(e => {
      return window.setTimeout(DBHelper.processForm(formData), 60000);
    })

  }




}